# turing-test

reference info and notes

## high level, important ideas (tm)

- androgynous android boy
- overall look is inspired by Jitomi Monoe
  - but less frowning
  - less ^ more -
- techwear aesthetic (but more colorful)
  - I don't really like the inclination towards black and earth tones

## Head

### Eyes

Twist on heterochromia, one eye circular w/ something
like a reflecting telescope in it for seeing visible light and at
distances, the other eye is square or hexagonal with the texture of a radar
array.  

#### Telescopic eye

- <img src="/mirrored-camera-lens.jpg" alt="mirrored camera lens" width="350"/>
- <img src="/mirrored-telescope.jpg" alt="mirrored telescope" width="350"/>
- <img src="/mirrored-telescope-labeled.jpg" alt="mirrored telescope with labels" width="350"/>

  - this last one is just so you have a better idea of what you are looking at

- in human terms, the primary mirror would be like the iris and the secondary mirror would be the pupil

#### Radar eye

- <img src="/nose-cone-radar.jpg" alt="radar array in jet's nose cone" width="350"/>

  - I like the color and texture of this one most

- <img src="/radar-shapes.jpg" alt="mirrored camera lens" width="350"/>

  - the middle pic here is a close up of something similar to the first pic, better pic of texture

- <img src="/Cobra-Judy-Phased-Array.jpg" alt="big radar array" width="350"/>

  - this one has a cool texture, this one would be good for a hexagonal design

- in human terms, the radar is the iris (actually square iris), and there could be a dot in the middle as the pupil
- for now let's work toward a square design with a texture like the first two radar pics

The square eye could have a dot in the middle if that makes it look a little more
natural, the dot could be a lidar sensor or something (since lidar sensors
basically just look like dots).  Eyes should probably be
on the larger side, more like Mizuki's model than Huechi's to show off details
in the eyes.  Colors for the circular eye could be lighter shades of blue and
the square eye would be more gold or copper color.  



### Hair

- Light blond hair (I was thinking platinum blond, but that might be a shade or two too light)
  - Tried getting some hex codes, couldn't quite get what I wanted but #fffb87 is close
- Under cut with messy top
- slightly curly, not something that would be described as curly, maybe more like wavy

- <img src="/hedi-slimane.jpg" alt="Hedi Slimane" width="350"/>

  - I think Hedi is closer to what I'm looking for, unfortunately every pic of Hedi is black and white

- <img src="/james-charles.jpg" alt="James Charles" width="350"/>

  - easier to see and has the right idea, but it's a little too curly

## Top

### Jacket

Basically that Arc'teryx shell I own.  

### Cross-body bag

Similar to the Black Ember Citadel in concept, but it's a cross-body bag
instead of a backpack.  

## bottom

Three options here:
- tights with some kind of straps for accessories
- something more like the P30A-DS from ACRNM w/ attach points for accessories
- a bit of a mix between the first two, a tighter fitting track pant with
similar attach points to option 2
